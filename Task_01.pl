%% Ryan Zimcosky
%% Task_01 -- it is 4 in the morning.

%% List of celebrities.
celebrity(trump).
celebrity(jared_from_subway).
celebrity(martha_stewart).

%% List of presidents.
president(trump).
president(nixon).
president(obama).
president(jfk).

%% List of people who have broken laws.
broke_laws(jared_from_subway).
broke_laws(trump).
broke_laws(martha_stewart).
broke_laws(nixon).
broke_laws(zodiac).
broke_laws(jack_ripper).

%% List of law breakers proven guilty.
got_caught(trump).
got_caught(jared_from_subway).
got_caught(martha_stewart).
got_caught(nixon).

%% People get imprisoned if they broke the law, 
%% got caught, and were never president.
imprisoned(X) :-
    broke_laws(X),
    got_caught(X),
    \+ president(X).

%% This guy also went to prison, even though he didn't break any laws.
imprisoned(andrew_dufresne).

%% He then escaped prison.
escapes(andrew_dufresne).

%% If someone was wrongfully imprisoned, and escaped, 
%% they have been shawshank redeemed, and text is displayed.
shawshank_redeemed(X) :-
    \+ broke_laws(X),
    imprisoned(X),
    escapes(X),
    nl, write('We should write a movie about this!'), nl.

%% If someone breaks a law and never gets caught, they are sneaky.
%% (and the only sneaky people in this database happen to be serial killers).
sneaky_serial_killer(X) :-
    broke_laws(X),
    \+got_caught(X).

%% Two people can be homies if they are law abiding presidents.
homies(X,Y) :-
    president(X), president(Y),
    \+broke_laws(X), \+broke_laws(Y),
    \+(X = Y).

%% Also, Richard and John are known friends.
homies(nixon, jfk).

%% Two people can also be homies if they were both imprisoned...
homies(X,Y) :-
    imprisoned(X),
    imprisoned(Y),
    \+(X = Y).
%% Or if they were both serial killers...
homies(X,Y) :-
    sneaky_serial_killer(X),
    sneaky_serial_killer(Y),
    \+(X = Y).
%% Or both celebrities that were never president.
homies(X,Y) :-
    celebrity(X),
    celebrity(Y),
    \+president(X),
    \+president(Y),
    \+(X = Y).

%% If a person on this database has no homies, they turn orange on comand.
orange(X) :-
    \+homies(X,Y).



