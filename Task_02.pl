%% 1. Butch is a killer.
killer(a).

%% 2. Mia and Marcellus are married.
married(b, c).

%% 3. Zed is dead.
dead(d).

%% 4. Marcellus kills everyone who gives Mia a footmassage.
kills(c, X) :- givesFootMassage(X, b).

%% 5. Mia loves everyone who is a good dancer.
loves(b, X) :- goodDancer(X).

%% 6. Jules eats anything that is nutritious or tasty.
eats2(e, X) :- nutritious(X); tasty(X).