# CS3060_FA2018_R04_ZIMCOSKY 

## Task 1

**__ Is Jared from Subway a celebrity? __**
```bash
| ?- celebrity(jared_from_subway).

(1 ms) yes
```

**__ Was JFK president? __**
```bash
| ?- president(jfk).

yes
```

**__ Who are all the people who have broken laws? __**
```bash
| ?- broke_laws(Who).

Who = jared_from_subway ? a

Who = trump

Who = martha_stewart

Who = nixon

Who = zodiac

Who = jack_ripper

(1 ms) yes
```

**__ Who all has been caught? __**
```bash
| ?- got_caught(Who).

Who = trump ? a

Who = jared_from_subway

Who = martha_stewart

Who = nixon

yes
```

**__ Who all has been imprisoned? __**
```bash
| ?- imprisoned(Who).

Who = jared_from_subway ? a

Who = martha_stewart

Who = andrew_dufresne

yes
```

**__ Andrew Dufresne has been imprisoned!? Has he broken any laws? __**
```bash
| ?- broke_laws(andrew_dufresne).

no
```

**__ Does Andrew escape prison? __**
```bash
| ?- escapes(andrew_dufresne).

yes
```

**__ Does he get redeemed from Shawshank? __**
```bash
| ?- shawshank_redeemed(andrew_dufresne).

We should write a movie about this!

yes
```
**__ Who are all the sneaky serial killers? __**
```bash
| ?- sneaky_serial_killer(Who). 

Who = zodiac ? a

Who = jack_ripper

yes
```

**__ What are all possible combinations of homies from these lists? __**
```bash
| ?- homies(All, Pairs).

All = obama
Pairs = jfk ? a

All = jfk
Pairs = obama

All = nixon
Pairs = jfk

All = jared_from_subway
Pairs = martha_stewart

All = jared_from_subway
Pairs = andrew_dufresne

All = martha_stewart
Pairs = jared_from_subway

All = martha_stewart
Pairs = andrew_dufresne

All = andrew_dufresne
Pairs = jared_from_subway

All = andrew_dufresne
Pairs = martha_stewart

All = zodiac
Pairs = jack_ripper

All = jack_ripper
Pairs = zodiac

All = jared_from_subway
Pairs = martha_stewart

All = martha_stewart
Pairs = jared_from_subway
```

**__ Is Trump orange? __**
```bash
| ?- orange(trump).

yes
```

## Reflection Questions 

1. **What was the major question or topic addressed in the classroom?**

How to get started with prolog, and write knowledge bases in the language.

2. **Summarize what you learned during this week of class. Note any particular features, concepts, code,or terms that you believe are important.** 

    I learned that prolog is a language used to solve problems automatically based on facts and rules

3. **What did you learn about the current language that is different from what you have previously experienced? Do you think it is a good difference or bad difference?**

    In previous languages, I always took a sort of step by step approach towards solving a problem, but python solves by itself, and you just have to define the problem itself and some facts for it to start approaching that problem with.

4. **Do you learn answers to any of the 5 fundamental questions we are asking about each programming language this week?**

  1. What is the typing model?
    Strongly statically typed

  2. What is the programming model?
    Logical declaritive language

  3. Is the language Interpreted or Compiled?
    compiled

  4. What are the decision constructs & core data structures?
    Facts, rules, querys and knowlege bases

  5. What makes this language unique?
  In this language rather than manipulating ingredients to bake a final cake, you define the ingredients and the cake, and prolog does the rest.

5. **What was the most difficult aspect of what you learned in class? Why was this the most difficult aspect? What can you do to make it easier for you?**
Trying to solve queries based on more derivative rules.